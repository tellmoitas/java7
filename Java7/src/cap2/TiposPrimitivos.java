/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cap2;

/**
 *
 * @author tell
 */
public class TiposPrimitivos {
    public static void main(String[] args) {
        char sexo = 'f';
        byte idade = 89;
        short codigo= 256;
        float nota = 9.4f;
        int alunos = 100, classes = 10;
        long habitantes = 9050100;
        double dolar = 2.62;
        boolean alternativa = false;
        
        System.out.println("\nsexo: " + sexo + " idade: " + idade + " codigo: " + codigo);
        System.out.println("\nnota: " + nota + " alunos: " + alunos + " classes: " + classes);
        System.out.println("\nhabitantes: " + habitantes + " dolar: " + dolar + " alternativa: "+ alternativa);
    }
}
