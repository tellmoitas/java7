package cap3;

import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
public class ContadorWhile {
    public static void main(String[] args) throws InterruptedException {
        try {
            int limite = Integer.parseInt(JOptionPane.showInputDialog("Quantidade?"));
            int contador = limite;
            while (contador >= 0) {
                System.out.println(contador);
                contador--;
                Thread.sleep(100);
            }
            System.out.println("Fim do contador");
            contador = 0;
            do {
                System.out.println(contador);
                contador++;
                Thread.sleep(100);
            } while (contador <= limite);
            System.out.println("Fim do contador progressivo");
        }
        catch(NumberFormatException erro){
            JOptionPane.showMessageDialog(null, erro.toString());
        }
        System.exit(0);
    }
}
