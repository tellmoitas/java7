/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cap3;

import javax.swing.JOptionPane;

/**
 *
 * @author tell
 */
public class If {
    public static void main(String[] args) {
        String resposta = JOptionPane.showInputDialog("Digite: S ou N");
        String men = "Resposta: " + resposta;
        
        if(!resposta.equals("S") && !resposta.equals("N")){
            System.out.println("Resposta inválida");
        }
        JOptionPane.showMessageDialog(null, men);
        System.exit(0);
    }
}
