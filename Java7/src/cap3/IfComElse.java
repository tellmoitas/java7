package cap3;

import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
public class IfComElse {
    public static void main(String[] args) {
        String aux = JOptionPane.showInputDialog("Forneça um número entre 0 e 10");
        if (aux != null){
            try {
                float x = Float.parseFloat(aux);
                if (x>=0 && x<=10) {
                    JOptionPane.showMessageDialog(null,x + " é um valor válido");
                } else {
                    JOptionPane.showMessageDialog(null, x + " é um valor inválido");
                }
            }
            catch (NumberFormatException erro){
                JOptionPane.showMessageDialog(null,"Digite um valor entre 0 e 10\n" + erro.toString());
            }
        }
        System.exit(0);
    }

}
