package cap3;

import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
public class IfComElseAninhado {
    public static void main(String[] args) {
        String diaDaSemana = JOptionPane.showInputDialog("Forneça um número entre 1 e 7");
        if (diaDaSemana != null) {
            try {
                int dia = Integer.parseInt(diaDaSemana);
                if (dia==1) diaDaSemana="Domingo";
                else if (dia==2) diaDaSemana = "Segunda";
                else if (dia==3) diaDaSemana = "Terça";
                else if (dia==4) diaDaSemana = "Quarta";
                else if (dia==5) diaDaSemana = "Quinta";
                else if (dia==6) diaDaSemana = "Sexta";
                else if (dia==7) diaDaSemana = "Sábado";
                else diaDaSemana = "Dia desconhecido";

                JOptionPane.showMessageDialog(null,diaDaSemana);

            } catch (NumberFormatException erro) {
                JOptionPane.showMessageDialog(null, "Forneça um número entre 1 e 7\n" + erro);
            }
        }
        System.exit(0);
    }
}
