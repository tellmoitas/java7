package cap3;

import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
class InstrucaoThrows {
    public static void main(String[] args) {
        float nota = Float.parseFloat(JOptionPane.showInputDialog("Forneça uma nota entre 0 e 10"));
        try{
            if(nota<0 || nota>10){
                throw new Exception("Fora da faixa");
            }
        }
        catch(Exception erro){
            JOptionPane.showMessageDialog(null, erro.toString());
        }
        finally {
            JOptionPane.showMessageDialog(null, "Você digitou " + nota);
        }
        System.exit(0);
    }
}
