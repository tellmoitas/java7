package cap3;


import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
public class SwitchCase {
    public static void main(String[] args) {
        String diaDaSemana = JOptionPane.showInputDialog("Digite um numero entre 1 e 7");
        if (diaDaSemana!=null) {
            try {
                int dia = Integer.parseInt(diaDaSemana);
                System.out.println(dia);
                String nDia = "";
                switch (dia) {
                    case 1:
                        nDia = "Domingo"; break;
                    case 2:
                        nDia = "Segunda"; break;
                    case 3:
                        nDia = "Terça"; break;
                    case 4:
                        nDia = "Quarta"; break;
                    case 5:
                        nDia = "Quinta"; break;
                    case 6:
                        nDia = "Sexta"; break;
                    case 7:
                        nDia = "Sábado"; break;
                    default:
                        nDia = "Dia desconhecido";
                }
                JOptionPane.showMessageDialog(null, nDia);
            } catch (NumberFormatException erro) {
                JOptionPane.showMessageDialog(null, erro);
            }
        }
    }
}
