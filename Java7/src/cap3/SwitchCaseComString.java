package cap3;


import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
public class SwitchCaseComString {
    public static void main(String[] args) {
        String sistema = JOptionPane.showInputDialog("Digite: Windows ou Linux");
        if (sistema!=null) {
            try {
                switch (sistema) {
                    case "Windows":
                        sistema = "Digitou Windows"; break;
                    case "Linux":
                        sistema = "Digitou Linux"; break;
                    default:
                        sistema = "Não digitou nada";
                }
                JOptionPane.showMessageDialog(null, sistema);
            } catch (NumberFormatException erro) {
                JOptionPane.showMessageDialog(null, erro);
            }
        }
    }
}
