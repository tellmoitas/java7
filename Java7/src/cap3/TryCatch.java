package cap3;

import javax.swing.*;

/**
 * Created by tell on 22/05/2018.
 */
public class TryCatch {
    public static void main(String[] args) {
        String sNum1 = JOptionPane.showInputDialog("Número 1: ");
        if (sNum1 == null)
            System.exit(0);
        String sNum2 = JOptionPane.showInputDialog("Número 2: ");
        if (sNum2 == null)
            System.exit(0);
        try {
            int num1 = Integer.parseInt(sNum1);
            int num2 = Integer.parseInt(sNum2);
            JOptionPane.showMessageDialog(null,"Soma: " + (num1+num2));
            JOptionPane.showMessageDialog(null,"Subtração: " + (num1-num2));
            JOptionPane.showMessageDialog(null,"Divisão: " + (num1/num2));
            JOptionPane.showMessageDialog(null,"Multiplicação: " + (num1*num2));

        }
        catch(ArithmeticException erro){
            JOptionPane.showMessageDialog(null, "Erro de divisão por zero\n" +
                    erro.toString(),"\nErro", JOptionPane.ERROR_MESSAGE);
        }
        catch (NumberFormatException erro){
            JOptionPane.showMessageDialog(null, "Digite apenas inteiros\n" +
                    erro.toString(),"\nErro", JOptionPane.ERROR_MESSAGE);
        }
        finally {
            JOptionPane.showMessageDialog(null,"Final de Execução");
        }
    }
}
